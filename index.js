const path = require('path');
const fs = require('fs');

const Reader = require("./src/Reader");
const BB = require("./src/BranchAndBound");
const {glouton} = require("./src/Glouton");
const {benchMark} = require("./src/Bench");

const BASES_DIR = "./Data/Bases";
let bench = false;
let SCENARIO_BASES;
let SCENARIO_ENTREPRISE;
let ALGO;


function help() {
    console.log("Usage:");
    console.log("\tnode index.js SCENARIO_BASES SCENARIO_ENTREPRISE [algo]");
    console.log("\tAlgo:");
    console.log("\t - 1: Glouton");
    console.log("\t - 2: Binary tree");
    console.log("\t - Rien: Lancement du benchmark");
    process.exit(1);
}

function myError(str) {
    console.log(str);
    help();
}

function getArgs() {
    let i = 0;
    process.argv.forEach(function (val, index) {
        if (index === 2)
            if (fs.lstatSync(val).isFile())
                SCENARIO_BASES = val;
            else
                myError("The scenario bases is not a file");
        else if (index === 3)
            if (fs.lstatSync(val).isFile())
                SCENARIO_ENTREPRISE = val;
            else
                myError("The scenario 'entreprise' is not a file");
        else if (index === 4)
            ALGO = val;
        i = index;
    });
    if (i === 3)
        bench = true;
    else if (i !== 4)
        help();
}

async function main() {
    getArgs();
    let scenarioBases = await Reader.readScenarioBase(SCENARIO_BASES);
    let scenarioEnt = await Reader.readScenarioEntreprise(SCENARIO_ENTREPRISE);
    let bases = [];


    for (const base of scenarioBases.bases) {
        let basePath = path.join(BASES_DIR, base);
        bases.push(await Reader.readBase(basePath));
    }
    // console.log(bases);
    // console.log(scenarioEnt);

    if (bench)
        benchMark(scenarioEnt, bases);
    else {
        console.time("Algo");
        switch (ALGO) {
            case '1':
                glouton(bases, scenarioEnt.companies);
                break;
            case '2':
                new BB.BinaryTree(scenarioEnt.companies, bases);
                break;
            default:
                console.log("[ERROR] This algo doesn't exist !");
                break;
        }
        console.timeEnd("Algo");
    }
}

main().then(() => {
    process.exit(0);
}).catch(e => console.error(e));
