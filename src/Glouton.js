function addCompanies(res, tab) {
    tab.forEach(ent => {
        if (res.indexOf(ent) === -1)
            res.push(ent);
    });
}

function glouton(bases, companies, print = true) {
    let currentCompany = "";
    let foundCompanies = [];
    let choice = [];
    let totalCost = 0;
    let totalBases = [];

    while (companies.length) {
        currentCompany = companies[companies.length - 1];

        if (foundCompanies.indexOf(currentCompany) === -1) {
            bases.forEach((currentBase, index) => {
                if (currentBase.companies.indexOf(currentCompany) !== -1) {
                    choice.push({
                        cost: currentBase.cost,
                        listEnt: currentBase.companies,
                        name: currentBase.name,
                        indexBase: index
                    });
                }
            });

            choice.sort((a, b) => (a.cost > b.cost) ? 1 : ((b.cost > a.cost) ? -1 : 0));
            bases.splice(choice[0].indexBase, 1);
            addCompanies(foundCompanies, choice[0].listEnt);
            totalCost += choice[0].cost;
            totalBases.push(choice[0].name);
            choice = [];
        }
        companies.pop();
    }
    if (print) {
        console.log(foundCompanies);
        console.log("Number of companies: " + foundCompanies.length);
        console.log("Total cost: " + totalCost);
        console.log("Bases used: ", totalBases)
    }
    return {
        nbCompanies: foundCompanies.length,
        totalCost: totalCost,
        nbBases: totalBases.length
    }
}

module.exports.glouton = glouton;
