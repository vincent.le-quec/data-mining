const fs = require('fs');
const {execSync} = require('child_process');

const Glouton = require("./Glouton");
const BB = require("./BranchAndBound");

const BENCH_IT = 50;

function writeFile(glouton = [], binaryTree = [], info = []) {
    let labels = [];
    for (let i = 0; i < glouton.length; i++)
        labels.push(i);

    let data = `
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Benchmark</title>
</head>
<body>
<canvas id="myChart"></canvas>
<p>Moyenne du glouton: ${(glouton.reduce((a, b) => parseFloat(a) + parseFloat(b), 0.0) / glouton.length) || 0} ms</p>
<p>Moyenne du Branch& Bound: ${(binaryTree.reduce((a, b) => parseFloat(a) + parseFloat(b), 0.0) / binaryTree.length) || 0} ms</p>
<hr>
<canvas id="myBench"></canvas>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script>
    let ctx = document.getElementById('myChart').getContext('2d');
    new Chart(ctx, {
        type: 'line',
        data: {
            labels: [${labels}],
            datasets: [
            {
                label: 'Glouton',
                borderColor: 'blue',
                data: [${glouton}]
            },
            {
                label: 'Arbre Binaire',
                borderColor: 'green',
                data: [${binaryTree}]
            },
            ]
        },
        options: {
            title: {
                display: true,
                text: "Temps d'execution des différents algorithmes"
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Temps en ms'
                    }
                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: 'Itérations'
                    }
                }]
            }
        }
    });
    
    let ctxBench = document.getElementById('myBench').getContext('2d');
    new Chart(ctxBench, {
        type: 'radar',
        data: {
            labels: ["Number of Companies", "Total cost", "Number of bases used", "Execution time (ms)"],
            datasets: [
                {
                    label: 'Glouton',
                    borderColor: 'blue',
                    data: [${info[0].nbCompanies}, ${info[0].totalCost}, ${info[0].nbBases}, ${info[0].time}]
                },
                {
                    label: 'Branch & Bound',
                    borderColor: 'green',
                    data: [${info[1].nbCompanies}, ${info[1].totalCost}, ${info[1].nbBases}, ${info[1].time}]
                },
            ]
        },
        options: {
            title: {
                display: true,
                text: "Représentation des données des différents algorithmes"
            },
            tooltips: {
                enabled: true,
                callbacks: {
                    label: function (tooltipItem, data) {
                        return data.datasets[tooltipItem.datasetIndex].label + ' : ' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                    }
                }
            }
        }
    });
</script>
</body>
</html>
`;
    fs.writeFileSync("bench.html", data);
    let start = (process.platform === 'darwin' ? 'open' : process.platform === 'win32' ? 'start' : 'xdg-open');
    execSync(start + " bench.html");
}

/**
 * Return elapsed time from start in ms
 * @param start
 * @returns {string}
 */
function elapsedTime(start) {
    // divide by a million to get nano to milli
    let elapsed = process.hrtime(start)[1] / 1000000;
    return elapsed.toFixed(6);
}

/**
 * Benchmark
 * @param scenarioEnt
 * @param bases
 */
function benchMark(scenarioEnt, bases) {
    // Time in ms
    let glouton = [];
    let bb2 = [];
    let info = [];
    let start;
    let tmpBases;
    let tmpComp;

    for (let i = 0; i < BENCH_IT; i++) {
        tmpBases = bases.slice();
        tmpComp = scenarioEnt.companies.slice();
        start = process.hrtime();
        Glouton.glouton(tmpBases, tmpComp, false);
        glouton.push(elapsedTime(start));

        tmpBases = bases.slice();
        tmpComp = scenarioEnt.companies.slice();
        start = process.hrtime();
        new BB.BinaryTree(tmpComp, tmpBases, false);
        bb2.push(elapsedTime(start));
    }

    // Info
    tmpBases = bases.slice();
    tmpComp = scenarioEnt.companies.slice();
    start = process.hrtime();
    info.push(Glouton.glouton(tmpBases, tmpComp, false));
    info[0].time = elapsedTime(start);

    tmpBases = bases.slice();
    tmpComp = scenarioEnt.companies.slice();
    start = process.hrtime();
    info.push(new BB.BinaryTree(tmpComp, tmpBases, false).getInfo());
    info[1].time = elapsedTime(start);

    writeFile(glouton, bb2, info);
}

module.exports.benchMark = benchMark;
