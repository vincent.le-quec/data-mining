const fs = require('fs');
const readline = require('readline');

function createReadline(filename) {
    // Note: we use the crlfDelay option to recognize all instances of CR LF
    // ('\r\n') in input.txt as a single line break.
    return readline.createInterface({
        input: fs.createReadStream(filename),
        crlfDelay: Infinity
    });
}

/**
 * Read base file & return object
 * @param filename
 * @returns {Promise<{companies: [], cost: number, size: number}>}
 */
async function readBase(filename) {
    let rl = createReadline(filename);
    let obj = {
        name: filename,
        cost: 0,
        size: 0,
        companies: []
    };
    let i = 0;

    for await (const line of rl) {
        if (line) {
            if (i === 0) obj.cost = Number.parseInt(line);
            else if (i === 1) obj.size = Number.parseInt(line);
            else obj.companies.push(line);
            i++;
        }
    }
    return obj;
}

/**
 * Read scenario base & return object
 * @param filename
 * @returns {Promise<{bases: [], size: number}>}
 */
async function readScenarioBase(filename) {
    let rl = createReadline(filename);
    let obj = {
        size: 0,
        bases: []
    };
    let i = 0;

    for await (const line of rl) {
        if (line) {
            if (i === 0) obj.size = line;
            else obj.bases.push(line);
            i++;
        }
    }
    return obj;
}

/**
 * Read scenario entreprise & return object
 * @param filename
 * @returns {Promise<{companies: [], size: number}>}
 */
async function readScenarioEntreprise(filename) {
    let rl = createReadline(filename);
    let obj = {
        size: 0,
        companies: []
    };
    let i = 0;

    for await (const line of rl) {
        if (line) {
            if (i === 0) obj.size = line;
            else obj.companies.push(line);
            i++;
        }
    }
    return obj;
}

module.exports.readBase = readBase;
module.exports.readScenarioBase = readScenarioBase;
module.exports.readScenarioEntreprise = readScenarioEntreprise;

