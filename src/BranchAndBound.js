const DEBUG = false;

class Node {
    constructor(e = [], b = [], c = 0) {
        this.totalCompanies = e;
        this.totalBases = b;
        this.totalCost = c;
        this.left = null;
        this.right = null;
        this.compLeftToFind = true;
    }
}

function isInBase(companies, name) {
    return (companies.find((company) => company === name) !== undefined);
}

class BinaryTree {
    constructor(companies, listBases, print = true) {
        this.root = new Node();
        let toFind = [];

        for (let e of companies)
            toFind.push(e);

        // tri par coût croissant
        // this.listBasesSorted = listBases.sort((b1, b2) => b1.cost - b2.cost);

        this.listBasesSorted = listBases.sort((b1, b2) => {
            let nb1 = 0;
            let nb2 = 0;

            toFind.forEach((comp) => {
                if (isInBase(b1.companies, comp))
                    nb1++;
                if (isInBase(b2.companies, comp))
                    nb2++;
            });

            let a = (nb1 > 0) ? (b1.cost / nb1) : 0;
            let b = (nb2 > 0) ? (b2.cost / nb2) : 0;
            return a - b;
        });

        this.buildTree(this.root, toFind, this.listBasesSorted);
        if (DEBUG) {
            this.depthFirst("root", this.root);
            console.log("Construction de l'abre binaire terminée.");
        }

        this.finalCost = Infinity;
        this.finalCompanies = [];
        this.finalBases = [];

        this.searchResult(this.root, toFind.length);

        if (print) {
            console.log(this.finalCompanies);
            console.log("Number of companies: " + this.finalCompanies.length);
            console.log("Total cost: " + this.finalCost);
            console.log("Bases used: ", this.finalBases);
        }
        this.info = {
            nbCompanies: this.finalCompanies.length,
            totalCost: this.finalCost,
            nbBases: this.finalBases.length
        };
    }

    buildTree(tree, toFind, bases) {
        if (bases.length > 0) {
            let base = bases.shift();   // première base de la liste
            let companiesFound = [];    // liste des entreprises trouvées dans cette base
            let toFindNext = [];        // liste des entreprises à chercher à l'étape suivante

            toFind.forEach((comp) => {
                if (isInBase(base.companies, comp))
                    companiesFound.push(comp);
                else
                    toFindNext.push(comp);
            });

            if (companiesFound.length > 0) {  // Construction de la branche "oui", si on a trouvé au moins une entreprise dans la base
                let cost = tree.totalCost + base.cost;
                let tmpBases = tree.totalBases.slice();
                tmpBases.push(base.name);

                let tmpComp = tree.totalCompanies.slice();
                this.addCompanies(tmpComp, base.companies);

                if (DEBUG) {
                    console.log(base.name);
                    console.log("companiesFound :", companiesFound);
                    console.log("toFindNext", toFindNext);
                    console.log("totalBases", tmpBases);
                    console.log("totalCost", cost);
                }
                tree.left = new Node(tmpComp, tmpBases, cost);
                this.buildTree(tree.left, toFindNext, bases);
            }
            if (toFindNext.length > 0) {      // construction de la branche "non", s'il reste des entreprises à trouver
                tree.right = new Node(tree.totalCompanies, tree.totalBases, tree.totalCost);
                this.buildTree(tree.right, toFind, bases);
            }
            if (toFindNext.length === 0)
                tree.compLeftToFind = false;
        }
    }

    searchResult(tree, numComp) {
        if (tree != null) {
            if (!(tree.compLeftToFind) && (tree.totalCost < this.finalCost)) {
                this.finalCost = tree.totalCost;
                this.finalBases = tree.totalBases;
                this.finalCompanies = tree.totalCompanies;
            } else {
                this.searchResult(tree.left, numComp);
                this.searchResult(tree.right, numComp);
            }
        }
    }

    addCompanies(res, tab) {
        tab.forEach(ent => {
            if (res.indexOf(ent) === -1)
                res.push(ent);
        });
    }

    getInfo() {
        return this.info;
    }

    // Only to debug
    depthFirst(str, node) {
        if (node) {
            // Redef' ce que tu veux afficher
            console.log(str + " - ", node);
            this.depthFirst("left", node.left);
            this.depthFirst("right", node.right)
        }
    }
}

module.exports.BinaryTree = BinaryTree;
