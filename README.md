# Data-Mining

## Prérequis

### NodeJS
Installation:
```shell script
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install -y nodejs
```

## Utilisation
```shell script
Usage:
    node index.js SCENARIO_BASES SCENARIO_ENTREPRISE [algo]
    Algo:
        - 1: Glouton
        - 2: Binary tree
        - Rien: Lancement du benchmark
Exemple:
node index.js ./Data/Scenarios/Liste\ Bases/Liste\ Bases1.txt ./Data/Scenarios/Liste\ Entreprises/Liste\ Ent1.txt 1
```

### Glouton
Lancement du glouton
```shell script
npm run start:glouton
# OU
node index.js ./Data/Scenarios/Liste\ Bases/Liste\ Bases1.txt ./Data/Scenarios/Liste\ Entreprises/Liste\ Ent1.txt 1
```

- L'algorithme glouton recherche, pour chaque entreprise, et si l'entreprise n'a pas déjà été ajoutée lors de l'achat d'une base précédente, 
la base contenant cette entreprise qui coûte le moins cher, il n'y a donc pas de recherche par efficience à base de (Nombre d'entreprise sur la base / prix de la base).
- À chaque fois que l'on ajoute une entreprise, on enlève la base achetée de notre pool de bases restantes, on prends l'entreprise suivante dans la liste, 
et on effectue la même recherche avec les bases qui restent

### Branch & Bound (arbre binaire)
Lancement du BranchAndBound
```shell script
npm run start:bb
# OU
node index.js ./Data/Scenarios/Liste\ Bases/Liste\ Bases1.txt ./Data/Scenarios/Liste\ Entreprises/Liste\ Ent1.txt 2
```

- L'algorithme Brand & Bound construit un arbre binaire d'une hauteur égale au nombre de bases proposées par le scénario exécuté. 
Un noeud de cet arbre est constitué de la liste des entreprises à chercher, les bases choisies ainsi que leur coût total, et d'un booléen indiquant si toutes les entreprises ont été trouvées. 
Chaque noeud est construit de la manière suivante : pour une base donnée, la branche gauche constitue le cas où l'on choisit d'ajouter cette base à notre liste, si celle-ci contient au moins une des entreprises recherchées. La branche droite constitue le cas où cette base ne sera pas choisie. S'il ne reste plus d'entreprises à trouver ou si toutes les bases ont été parcourues, le noeud devient une feuille.
- Une fois l'arbre construit, l'algorithme le parcourt et identifie les feuilles dans lesquelles toutes les entreprises recherchées sont présentes dans les bases choisies par la feuille. Il compare alors leur coût total afin de récupérer la liste de bases coûtant le moins cher.
- L'arbre construit par cet algorithme ne contient pas toutes les listes possibles de bases à choisir car une base traitée dans un noeuf ne peut pas l'être dans un autre. Ainsi l'ordre dans lequel on traite les bases influe sur les résultats trouvés dans les feuilles.
- Trier la liste des bases du scénario selon leur coût est l'ordre le plus simple mais pas le plus optimal. Nous avons choisi de trier les bases en fonction de leur coût mais aussi du nombre d'entreprises recherchées qu'elles contiennent. Par exemple pour une base de coût 45 contenant 3 des entreprises recherchées, on calcule 45/3=15. Cette base sera traitée avant une autre base dont un calcul similaire donnera un résultat plus grand.

### Benchmark
Lancement du Benchmark
```shell script
npm run start:bench
# OU
node index.js ./Data/Scenarios/Liste\ Bases/Liste\ Bases1.txt ./Data/Scenarios/Liste\ Entreprises/Liste\ Ent1.txt
```
- L'utilisation de cette commande vous permet de générer un graphique du temps que les algorithmes mettent pour N itérations (ici, N = 50) et un second, de type radar, pour comparer l'algorithme glouton au Branch & Bound. Normalement, ce dernier va s'afficher dans votre navigateur par défaut, si ce n'est pas le cas vous pouvez ouvrir [bench.html](./bench.html). Vous pouvez déselectionner une courbe en cliquant sur la légende si vous le souhaitez ainsi que mettre le curseur sur les points pour avoir la valeur.

- Comme vous pouvez le voir, lors de la 1ère itération, le temps d'exécution est plus long que par la suite. Ceci est dû à la mise en place de l'environnement tel que l'allocation de mémoire etc... On peut aussi voir quelques pics à cause d'autres processus qui tournent en plus de notre programme (ex: Google Chrome...). Cependant, on voit très bien que le Glouton met moins de temps à résoudre le problème, mais comme le montre les tests ci-dessous, on obtient de meilleur résultats via le Branch&Bound. 

On peut modifier le nombre d'itérations dans Bench.js (l.7) avec BENCH_IT.

#### Tests

- Scénario 1 : Base 1 & Ent 1 

|                | Nombre de compagnies | Coût total | Nombre de bases utilisées | Temps d'execution (ms)|
|:--------------:|:--------------------:|:----------:|:-------------------------:|:-----------------:|
|     Glouton    |          146         |     139    |             5             |       0.3287      |
| Branch & Bound |          124         |     99     |             3             |       2.8940      |

- Scénario 2 : Base 2 & Ent 2

|                | Nombre de compagnies | Coût total | Nombre de bases utilisées | Temps d'execution (ms) |
|:--------------:|:--------------------:|:----------:|:-------------------------:|:-----------------:|
|     Glouton    |          118         |     57     |             2             |       0.3852      |
| Branch & Bound |           85         |     26     |             1             |       1.6542      |

- Scénario 3 : Base 3 & Ent 3 

|                | Nombre de compagnies | Coût total | Nombre de bases utilisées | Temps d'execution (ms) |
|:--------------:|:--------------------:|:----------:|:-------------------------:|:-----------------:|
|     Glouton    |          119         |     107    |             4             |       0.6242      |
| Branch & Bound |          102         |     65     |             3             |       1.9820      |

## Auteurs

* **Vincent LE QUEC** - [Venatum](https://gitlab.com/vincent.le-quec)
* **Julien DOUET** - [juliendoueta8](https://gitlab.com/juliendoueta8)
* **Elia MOREAU** - [Yiniie](https://gitlab.com/Yiniie)
